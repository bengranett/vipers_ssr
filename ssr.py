import numpy as N
import logging
import cPickle as pickle
import utils

from sklearn.neighbors import KDTree


def do_count_list(tree, data, r):
    """ """
    count = tree.query_radius(data,r, count_only=True)
    return count

def get_rmax(tree, data, k=100):
    """ """
    d,m = tree.query(data, k)

    rmax = N.zeros(len(d))
    for i in range(len(d)):
        rmax[i] = N.max(d[i])

    return rmax


class SSR(object):
    """ """

    def __init__(self, norm='rank'):
        """ """
        self.norm = norm

        # savefile = '%s/ssrdata_%s.pickle'%(tmpdir,tag)
        # self.info = {'configuration': configuration,
                    # 'savefile': savefile,
                    # 'tag': tag}

    def load(self, success_flag=None, phot_data=None):
        """ """

        self.data = {}
        # self.data['zflg'] = zflg
        # self.data['zspec'] = zspec
        self.data['phot'] = phot_data
        self.data['success'] = success_flag > 0
        # self.data['select'] = selection.apply(self.info['configuration'], zflg)

    def transform(self, data, **params):
        """ """
        if self.norm == 'rank':
            pval = utils.rank_transform(data, **params)
        elif self.norm == 'stddev':
            try:
                b = self.stats
            except AttributeError:
                self.stats = None
            pval, self.stats = utils.stddev_transform(data, self.stats)

        return pval.transpose()


    def train(self, data=None):
        """ """
        if data is not None:
            self.data = data

        self.data['pval'] = self.transform(self.data['phot'])

        targ_data = self.data['pval']
        succ_data = self.data['pval'][self.data['success']]

        self.data['tree_targ'] = KDTree(targ_data)
        self.data['tree_succ'] = KDTree(succ_data)

        # self.save()

    def save(self):
        """ """
        savefile = self.info['savefile']
        stuff = (self.data, self.info)
        pickle.dump(stuff, file(savefile,'w'))
        logging.info("Wrote data file: %s", savefile)

    # def load(self, savefile=None):
    #     """ """
    #     if savefile is not None:
    #         self.savefile = savefile
    #     self.data, self.info = pickle.load(file(self.savefile))
    #     logging.info("Loaded data from file: %s", self.savefile)

    def compute(self, data=None, knn=100, return_rmax=False):
        """ """
        tree_targ = self.data['tree_targ']
        tree_succ = self.data['tree_succ']

        if data is None:
            data_r = tree_targ.data
        else:
            data_r = self.transform(data, ref=self.data['phot'])

        rmax = get_rmax(tree_targ, data_r, k=knn)
        logging.debug("rmax: %s",N.percentile(rmax,[10,50,90]))

        logging.debug("query 1")
        count = do_count_list(tree_targ, data_r, rmax)
        logging.debug("counts: %i %f %s", len(count),N.median(count),N.percentile(count,[10,90]))

        logging.debug("query 2")
        count2 = do_count_list(tree_succ, data_r, rmax)
        logging.debug("counts: %i %f %s", len(count2),N.median(count2),N.percentile(count2,[10,90]))

        w = count2*1./count

        if return_rmax:
            return w, rmax
        return w
