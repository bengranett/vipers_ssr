import os
import pylab
import numpy as N
import argparse
import logging
import utils
from catalogue import Catalogue
from ssr import SSR
import ssr_by_quadrant
import selection
from scipy.ndimage import gaussian_filter1d

def main(p1='photo_z', p2='i_T07', labels=None, norm='rank', knn=100, vmin=0.5, vmax=1., sample='galaxyagn', cmap='viridis_r'):
    """ """

    path = "plots/SR_%s_%s_%s.pdf"%(p1,p2,norm)
    path = path.replace(" ","")
    if os.path.exists(path):
        logging.info('File already exists. Won\'t overwrite. %s', path)
        return

    pnames = p1,p2
    if labels is None:
        labels = []
        for p in pnames:
            labels.append("$%s_{%s}$"%tuple(p.split("_")))


    logging.info("Labels: %s",labels)

    do_quad_ssr = False

    if p1 == 'Q':
        do_quad_ssr = True
        pnames = p2,p2
        qi = 0
    elif p2 == 'Q':
        do_quad_ssr = True
        pnames = p1,p1
        qi = 1

    if do_quad_ssr:
        # Load quadrant average completeness
        quad_ssr = ssr_by_quadrant.get_ssr_by_quadrant(sample)


    with Catalogue(dbfile='vipers.db') as C:
        C.load('spec',['num','pointing','quadrant','zspec','zflg'])
        C.load('phot',['num','vmmpsflag','newflag','i_T07'])
        C.load('photsed',['num','photo_z','M_Uj','M_B','M_V'])

        cmd = """select pointing,quadrant,zspec, zflg, %s, %s
        from spec as S 
        join phot as P 
        join photsed as D 
        where S.num=P.num and S.num=D.num and 
        P.vmmpsflag='S' and P.newflag=1 and zflg >= 0 and zflg < 20 and 
        i_T07>0 and i_T07<100 and 
        M_B<0 and M_B>-30 and
        M_V<0 and M_V>-30 and
        M_Uj<0 and M_Uj>-30;"""%pnames

        data = C.query(cmd)
        logging.info("Results: %i",len(data))

    pointing,quad,zspec,zflg,param1,param2 = utils.unzip(data)

    if do_quad_ssr:
        q = N.zeros(len(zspec),dtype='d')
        for i in range(len(q)):
            point = ssr_by_quadrant.unique_pointing(pointing[i])
            pointkey = ssr_by_quadrant.pointing_key(point,quad[i])
            q[i] = quad_ssr[pointkey]

        if qi == 0:
            param1 = q
        else:
            param2 = q

    succ = selection.apply(sample, zflg)

    phot = N.transpose([param1, param2])

    S = SSR(norm=norm)
    S.load(success_flag=succ, phot_data=phot)
    S.train()

    comp = S.compute(phot, knn=knn)
    print "mean compl",N.mean(comp)

    xlow,xhigh = N.percentile(param1,[0.5,99.8])
    ylow,yhigh = N.percentile(param2,[0.5,99.8])

    x = N.linspace(xlow,xhigh,50)
    y = N.linspace(ylow,yhigh,50)
    xc = (x[1:]+x[:-1])/2.
    yc = (y[1:]+y[:-1])/2.

    xx,yy = N.meshgrid(xc,yc)
    shape = xx.shape
    xx = xx.flatten()
    yy = yy.flatten()
    grid = N.transpose([xx,yy])

    f = S.compute(grid,knn=knn)

    f = f.reshape(shape)

    h,ex,ey = N.histogram2d(param2,param1,(y,x))
    hsmoo = gaussian_filter1d(h,2)

    plev = N.array([0.99, 0.90,0.5,0.1])
    levels = utils.lowerwater(hsmoo, plev)


    ii = hsmoo < 2
    f[ii] = float('nan')

    ok = N.isfinite(f)
    print "range:",f[ok].min(),f[ok].max()

    ext = (x.min(),x.max(),y.min(),y.max())

    pylab.figure(figsize=(4,3))

    pylab.imshow(f, extent=ext, vmin=vmin, vmax=vmax, aspect='auto', origin='lower', interpolation='nearest', cmap=cmap)
    pylab.colorbar(label="Completeness")
    pylab.contour(hsmoo, levels=levels, colors='k', extent=ext, origin='lower')

    pylab.xlabel(labels[0])
    pylab.ylabel(labels[1])

    pylab.subplots_adjust(left=0.12,bottom=0.12,top=0.95)

    pylab.savefig(path)
    pylab.close()


if __name__=="__main__":
    logging.basicConfig(level=logging.INFO)

    cmap = pylab.get_cmap('rainbow',10)
    

    main('M_Uj - M_V','M_B',labels=('$U - V$','$B$'), knn=100,cmap=cmap)
    main('M_Uj - M_V','i_T07',labels=('$U - V$','$i$'), knn=100,cmap=cmap)

    main('photo_z','M_Uj - M_V',labels=('$photoz$','$U - V$'),cmap=cmap)
    main('photo_z','i_T07',labels=('$photoz$','$i$'),cmap=cmap)

    main("Q","i_T07",labels=('Mean quadrant completeness','$i$'),cmap=cmap)

