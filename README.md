# README #

## Options ##
```
#!bash

python main.py -h

usage: main.py [-h] [--outdir OUTDIR] [--sample {galaxy,galaxyagn,galaxylow}]
               [--knn KNN] [--transform {rank,stddev}] [--params p [p ...]]

VIPERS spectroscopic completeness calculator

optional arguments:
  -h, --help            show this help message and exit
  --outdir OUTDIR       Output directory to store SSR catalogue.
  --sample {galaxy,galaxyagn,galaxylow}
                        Sample for SSR calculation. Options include: "galaxy"
                        flags 2-10, "galaxyagn" flags 2-10 & 12-20,
                        "galaxylow" flags 1.5-10. (Default galaxyagn)
  --knn KNN             Number of nearest neighbors to average over. (Default
                        100)
  --transform {rank,stddev}
                        Standardization transformation to apply for computing
                        distances in the parameter space. (Default rank)
  --params p [p ...]
```

The ```sample``` parameter describes the sample selection.  It should be clear looking at the code in ```selection.py```:
```
#!python
    if mode=='galaxy':
        if (zqual >= 2)&(zflg<10)&(zflg>0):
            return True
    elif mode=='galaxyagn':
        if (zqual >= 2)&(zflg<20)&(zflg>0):
            return True
    elif mode=='galaxylow':
        if (zflag >= 1.5)&(zflg<10)&(zflg>0):
            return True
```


## Running ##
```
#!bash
# compute mean SSR per quadrant
python main.py

# make a plot
python plot2d.py

```