import subprocess


def describe():
    """ """
    try:
        label = subprocess.check_output("git describe --always --long --dirty".split())
    except OSError:
        label = "<< Error: git executable not found >>"
    except subprocess.CalledProcessError:
        label = "<< Error: git describe failed >>"
    except:
        raise
    return label.strip()


if __name__=="__main__":
    print "hash:",describe() 