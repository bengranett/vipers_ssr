import sys
import numpy as N
import catalogue
import ssr_by_quadrant
import ssr
import selection
import logging
import argparse
import git
import time
from collections import OrderedDict

command_line = " ".join(sys.argv)

limits = {'M_Uj': (-30,0),
        'M_B': (-30,0),
        'M_V': (-30,0),
        'i_T07': (0,30),
    }

def main(outdir='.', sample='galaxyagn', knn=100, transform='rank', params=None, dbfile='vipers.db'):
    """ """
    logging.basicConfig(level=logging.DEBUG)

    assert(selection.validate(sample))

    tag = "n%i"%len(params)

    header = OrderedDict()
    header["sample"] = sample
    header["knn"] = knn
    header["transform"] = transform
    header["params"] = ", ".join(params)
    header["nparams"] = len(params)

    do_quad_ssr = False

    param_list = []
    constraints_list = []
    for pname in params:
        if pname == 'Q':
            do_quad_ssr = True
            continue
        param_list.append(pname)

        for sub in pname.split("-"):
            if not sub in limits:
                logging.critical("No limits known for parameter %s!", sub)
                lim = 0,100
            else:
                lim = limits[sub]
            constraints_list.append("%s between %f and %f"%(sub, lim[0], lim[1]))

    param_str = ",".join(param_list) 
    constraints_str = " and\n".join(constraints_list)

    if do_quad_ssr:
        # Load quadrant average completeness
        quad_ssr = ssr_by_quadrant.get_ssr_by_quadrant(sample)


    with catalogue.Catalogue(dbfile=dbfile) as C:
        C.load('spec',['num','pointing','quadrant','zspec','zflg'])
        C.load('phot',['num','vmmpsflag','newflag','i_T07'])
        C.load('photsed',['num','photo_z','M_Uj','M_B','M_V'])

        cmd = """select S.num, pointing, quadrant, zspec, zflg, %s
        from spec as S 
        join phot as P 
        join photsed as D 
        where S.num=P.num and S.num=D.num and 
        P.vmmpsflag='S' and P.newflag=1 and zflg >= 0 and zflg < 20 and 
        %s;"""%(param_str, constraints_str)

        logging.debug(cmd)

        results = C.query(cmd)
        print "Results:",len(results)
    if len(results)==0:
        logging.critical("DB query returned nothing!")
        exit()

    objid = []
    phot = []
    succ_flag = []
    for row in results:
        num,point,quad,zspec,zflg = row[:5]
        other_params = row[5:]

        # skip stars
        if selection.star(zflg, zspec):
            continue

        vec = []
        if do_quad_ssr:
            point = ssr_by_quadrant.unique_pointing(point)
            pointkey = ssr_by_quadrant.pointing_key(point,quad)
            q = quad_ssr[pointkey]
            vec = [q]

        for p in other_params:
            vec.append(p)

        objid.append(num)
        phot.append(vec)

        succ = 0
        if selection.select(sample, zflg):
            succ = 1

        succ_flag.append(succ)

    phot = N.array(phot)
    succ_flag = N.array(succ_flag)

    S = ssr.SSR(norm=transform)
    S.load(success_flag=succ_flag, phot_data=phot)
    S.train()

    comp = S.compute(phot, knn=knn)
    print "mean compl",N.mean(comp)


    outpath = "%s/ssr_%s_%s_%s.txt"%(outdir, sample, knn, tag)

    write_file(objid, comp, header=header, outpath=outpath, dbfile=dbfile)


def write_file(objid, ssr, header={}, outpath="ssr.txt", dbfile='vipers.db'):
    """ """
    ssr_lookup = {}
    for i in range(len(objid)):
        ssr_lookup[objid[i]] = ssr[i]

    with catalogue.Catalogue(dbfile=dbfile) as C:
        num = C.query("select num from spec;")

    header['nobj'] = len(num)
    header['nobj with SSR'] = len(ssr)
    header['Mean SSR'] = N.mean(ssr)
    header['Median SSR'] = N.median(ssr)
    header['Min SSR'] = N.min(ssr)
    header['Max SSR'] = N.max(ssr)

    with file(outpath,'w') as out:
        print >>out, "#", command_line
        print >>out, "#", time.asctime()
        print >>out, "#", git.describe()
        for key,value in header.items():
            print >>out, "# %s: %s"%(key,value)

        for objnum in num:
            i = objnum[0]
            v = 0
            if i in ssr_lookup:
                v = ssr_lookup[i]
            print >>out, i, v


if __name__=="__main__":
    parser = argparse.ArgumentParser(description='VIPERS spectroscopic completeness calculator')
    parser.add_argument('--outdir', default='.', help="Output directory to store SSR catalogue.")
    parser.add_argument('--sample', default='galaxyagn', choices=selection.sample_list, help="""Sample for SSR calculation. Options include: "galaxy" flags 2-10, "galaxyagn" flags 2-10 & 12-20, "galaxylow" flags 1.5-10. (Default galaxyagn)""")
    parser.add_argument('--knn', default=100, type=int, help="Number of nearest neighbors to average over. (Default 100)")
    parser.add_argument('--transform', default='rank', choices=('rank','stddev'), help="Standardization transformation to apply for computing distances in the parameter space. (Default rank)")
    parser.add_argument('--params', metavar='p', default=('Q','i_T07','M_B','M_Uj-M_V'), type=str, nargs='+')
    args = parser.parse_args()

    main(sample=args.sample, knn=args.knn, transform=args.transform, params=args.params, outdir=args.outdir)
